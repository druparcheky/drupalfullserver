FROM ubuntu

WORKDIR /var/www/html/

RUN set -eux

# update packages
RUN apt update && apt upgrade -y

# install basics
RUN export DEBIAN_FRONTEND=noninteractive ; \
    ln -fs /usr/share/zoneinfo/America/New_York /etc/localtime ; \
    apt-get install -y tzdata sudo curl unzip nfs-common openssh-server git ; \
    dpkg-reconfigure --frontend noninteractive tzdata

# install php
RUN apt -y install php8.1 php8.1-mbstring php-pear php-common php-mysql php-cgi php-mbstring php-curl php-gd php-xml php-xmlrpc

# # PHP.INI
# ENV PHP_INI_DIR /usr/local/etc/php
# RUN cp "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"

# GD WITH WEBP SUPPORT
# RUN apt install -y libwebp-dev libwebp6 webp libmagickwand-dev ;
# RUN docker-php-ext-configure gd --enable-gd --with-freetype --with-jpeg --with-webp ;
# RUN docker-php-ext-install gd ;

# install node
RUN apt -y install nodejs npm ; \
    npm install -g stable ; \
    npm install -g n ; \
    n stable ;

# install composer
RUN apt -y install composer
ENV PATH=~/.config/composer/vendor/bin:$PATH
RUN ln -fs /root/.config/composer/vendor/bin/drush /usr/local/bin/drush

# install drush-launcher
RUN composer global require drush/drush-launcher

# install apache
RUN apt install -y apache2 ; \
    echo "ServerName localhost" >> /etc/apache2/apache2.conf ; \
    sudo a2enmod rewrite
ENV APACHE_RUN_USER=www-data
ENV APACHE_RUN_GROUP=www-data
ENV APACHE_LOG_DIR=/var/log
ENV APACHE_PID_FILE=/var/run/apache2/apache2$SUFFIX.pid
ENV APACHE_RUN_DIR=/var/www/html
COPY configs/000-default.conf /etc/apache2/sites-available/000-default.conf

# install mysql
RUN sudo apt -y install mariadb-server mariadb-client ; \
    sudo /etc/init.d/mariadb start ; \
    mysql -u root -e "CREATE USER 'drupal'@'127.0.0.1' IDENTIFIED BY 'drupal';" ; \
    mysql -u root -e "GRANT ALL PRIVILEGES ON * . * TO 'drupal'@'127.0.0.1';" ; \
    mysql -u root -e "FLUSH PRIVILEGES;" ;

# entrypoint
EXPOSE 80 443 3306
COPY scripts/entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
SHELL ["/bin/bash", "-ec"]
CMD [ "/entrypoint.sh" ]