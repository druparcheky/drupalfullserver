#!/bin/bash

function usage {
       printf "Usage:\n"
       printf " -h                               Display this help message.\n"
       printf " -p 0,1                           Push to docker regitry if p=1 \n"
       exit 0
}

cd /home/user/sitios/drupalfullserver
if [ "$?" -ne 0 ]; then
       echo "BUILD failed"
       exit 1
fi
docker build --pull --rm -f "Dockerfile" -t carcheky/drupalfullserver:dev -t drupalfullserver "."
if [ "$?" -ne 0 ]; then
       echo "command failed"
       exit 1
fi

cd /home/user/sitios/carcheky
# docker run --rm --publish 80:80 --volume $(pwd):/var/www/html --env-file $(pwd)/.env carcheky/drupalfullserver:dev
docker compose down
docker compose up -d --build
cd /home/user/sitios/drupalfullserver

# while getopts h:p: opt; do
#        case "${opt}" in
#        h) usage ;;
#        p) push=${OPTARG} ;;
#        *)
#               printf "Invalid Option: $1.\n"
#               usage
#               ;;
#        esac
# done
# if [ -f .env ]
# then
#   . .env
#   docker scan --token $SNYK_TOKEN --login
#   docker scan carcheky/drupalfullserver:latest --accept-license
# fi

# if [ $push ];
# then
# docker push carcheky/drupalfullserver:dev
# fi
