#!/bin/bash

. /var/www/html/.env

if id "${LINUX_USER}" &>/dev/null; then
    echo 'user found'
else
    echo 'user not found, creating'
    groupadd --force --gid ${GID} ${LINUX_USER}
    useradd --no-log-init --uid ${UID} --gid ${GID} ${LINUX_USER}
    echo "${LINUX_USER}:password" | chpasswd
    install --directory --mode 0755 --owner ${LINUX_USER} --group ${GID} /home/${LINUX_USER}
    chown --changes --silent --no-dereference --recursive ${UID}:${GID} /home/${LINUX_USER}
    usermod -aG sudo ${LINUX_USER}
    echo "${LINUX_USER} ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers.d/nosudopassword
fi