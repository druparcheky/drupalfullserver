#!/bin/sh
set -eux
# . /var/www/html/.env
sudo /etc/init.d/mariadb start

if [ -f /var/www/html/entrypoint.sh ]
then
  bash /var/www/html/entrypoint.sh
fi

apache2 -D FOREGROUND